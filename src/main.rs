#![warn(clippy::all)]

use std::fs;
use std::time::Instant;
#[cfg(feature="backtraces")]
use std::backtrace::Backtrace;
// use std::collections::{HashSet, HashMap};
// use std::hash::{Hash, Hasher};
use std::str::FromStr;

use anyhow::Result;
use thiserror::Error;
// use logos::{Logos, Lexer};
// use ::bytecount;
// use ::permute::permute;

#[macro_export]
macro_rules! import_and_use_mod {
    ($mod_name:ident) => {
        mod $mod_name;
        #[allow(unused_imports)]
        use crate::$mod_name::*;
    };
}

import_and_use_mod!(util);
// import_and_use_mod!(intcode);

import_and_use_mod!(task_01);
import_and_use_mod!(task_02);
import_and_use_mod!(task_03);

#[allow(clippy::println_empty_string)]
fn main() -> Result<()> {
    println!("Disk I/O will be included in timers.");

    println!("");

    timer_create_run!(task_01_a);
    timer_create_run!(task_01_b);

    println!("");

    timer_create_run!(task_02_a);
    timer_create_run!(task_02_b);

    println!("");

    timer_create_run!(task_03_a);
    timer_create_run!(task_03_b);

    Ok(())
}
