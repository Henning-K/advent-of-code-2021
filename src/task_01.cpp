#include <iostream>
#include "task_01.hpp"

void say_hello() {
    std::cout << "Hello from task_01" << std::endl;
}

int add2(const int a, const int b) {
    return a+b;
}
