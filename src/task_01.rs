use super::*;

pub(crate) fn task_01_a() -> Result<usize> {
    let contents = fs::read_to_string("data/01.txt")?;
    count_depth_increases(&contents)
}

pub(crate) fn task_01_b() -> Result<usize> {
    let contents = fs::read_to_string("data/01.txt")?;
    count_depth_increases_threes(&contents)
}

fn count_depth_increases(inp: &str) -> Result<usize> {
    process(inp).map(|val| val.windows(2).filter(|&arr| arr[0]<arr[1]).count())
}

fn count_depth_increases_threes(inp: &str) -> Result<usize> {
    let threes: Result<Vec<i64>> = process(inp).map(|val| val.windows(3).map(|arr| arr.iter().sum()).collect());
    threes.map(|val| val.windows(2).filter(|&arr| arr[0] < arr[1]).count())
}

fn process(inp: &str) -> Result<Vec<i64>> {
    inp.trim()
        .lines()
        .map(|line| i64::from_str(line.trim()).map_err(|e| e.into()))
        .collect::<Result<Vec<i64>>>()
}

#[test]
fn test_01_a() {
    let data = r#"199
    200
    208
    210
    200
    207
    240
    269
    260
    263
    "#;
    let result = count_depth_increases(data).expect("Something went wrong in this test.");
    assert_eq!(7, result);
}

#[test]
fn test_01_b() {
    let data = r#"199
    200
    208
    210
    200
    207
    240
    269
    260
    263
    "#;
    let result = count_depth_increases_threes(data).expect("Something went wrong in this test.");
    assert_eq!(5, result);
}
