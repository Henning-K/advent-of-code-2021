use super::*;

pub(crate) fn task_02_a() -> Result<i64> {
    let contents = fs::read_to_string("data/02.txt")?;
    
    part_a(process(&contents))
}

pub(crate) fn task_02_b() -> Result<i64> {
    let contents = fs::read_to_string("data/02.txt")?;
    
    part_b(process(&contents))
}

fn part_a(inp: Result<Vec<(i64,i64)>>) -> Result<i64> {
    inp.map(|lines| {
        let (a,b) = lines.into_iter().fold((0,0), |acc, (a,b)| {
            (acc.0 + a, acc.1 + b)
        });
        a*b
    })
}

fn part_b(inp: Result<Vec<(i64,i64)>>) -> Result<i64> {
    inp.map(|lines| {
        let (horizontal_pos, depth, _aim) = lines.into_iter().fold((0, 0, 0), |acc, (a, b)| {
            match (a, b) {
                (0, val) => (acc.0, acc.1, acc.2 + val),
                (val, 0) => (acc.0 + val, acc.1 + val*acc.2, acc.2),
                (_, _) => unreachable!("anomalous line"),
            }
        });
        horizontal_pos * depth
    })
}

fn process(inp: &str) -> Result<Vec<(i64,i64)>> {
    inp.trim()
        .lines()
        .map(|line| {
            let l: Vec<_> = line.trim().split_whitespace().collect();
            i64::from_str(l[1]).map_err(|e| e.into()).map(|n| {
                match l[0] {
                    "forward" => (n, 0),
                    "up" => (0, -n),
                    "down" => (0, n),
                    _ => unreachable!("direction not recognized"),
                }
            })
        })
        .collect::<Result<Vec<_>>>()
}

#[test]
fn test_02_a() {
    let data = r#"forward 5
    down 5
    forward 8
    up 3
    down 8
    forward 2
    "#;
    let result = part_a(process(data)).expect("Something went wrong in this test.");
    assert_eq!(150, result);
}

#[test]
fn test_02_b() {
    let data = r#"forward 5
    down 5
    forward 8
    up 3
    down 8
    forward 2
    "#;
    let result = part_b(process(data)).expect("Something went wrong in this test.");
    assert_eq!(900, result);
}
