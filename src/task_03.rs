use super::*;


#[derive(PartialEq, Eq,Debug,Clone)]
struct Board {
    w: usize,
    h: usize,
    data: Vec<u8>
}

impl Board {
    fn new(h: usize, w: usize, data: Vec<u8>) -> Self {
        Board {w,h,data}
    }

    fn at(&self, i: usize, j: usize) -> u8 {
        self.data[&self.w * i + j]
    }
}

#[derive(PartialEq, Eq, Debug, Clone)]
enum ProcessingResult {
    A(Board),
    B(Vec<Vec<u8>>)
}

pub(crate) fn task_03_a() -> Result<i64> {
    part_a(process(fs::read_to_string("data/03.txt").map_err(|e| e.into()), true))
}

pub(crate) fn task_03_b() -> Result<i64> {
    part_b(process(fs::read_to_string("data/03.txt").map_err(|e| e.into()), false))
}

fn part_a(inp: Result<ProcessingResult>) -> Result<i64> {
    inp.map(|proc_rec| {
        let board = match proc_rec {
            ProcessingResult::A(b) => b,
            _ => unreachable!()
        };
        let gamma_epsilon_bits = (0..board.w).map(|j| {
            let zeroes_ones: (Vec<u8>, Vec<u8>) = (0..board.h).map(|i| {
                board.at(i, j)
            }).partition(|&c| c == 0);
            match zeroes_ones.0.len().cmp(&zeroes_ones.1.len()) {
                std::cmp::Ordering::Less => (1, 0),
                std::cmp::Ordering::Greater => (0, 1),
                std::cmp::Ordering::Equal => unreachable!()
            }
        }).collect::<Vec<(u8,u8)>>();
        // power consumption = gamma rate * epsilon rate
        vu_to_i64(&gamma_epsilon_bits.iter().cloned().map(|(a, _)| a).collect::<Vec<u8>>()) * vu_to_i64(&gamma_epsilon_bits.iter().cloned().map(|(_, b)| b).collect::<Vec<u8>>())
     })
}

fn part_b(inp: Result<ProcessingResult>) -> Result<i64> {
    inp.map(|proc_rec| {
        let vvu = match proc_rec {
            ProcessingResult::B(v) => v,
            _ => unreachable!()
        };
        // life support rating = oxygen generator rating    (most common bits in columns)
        //                     * CO2 scrubber rating        (least common bits in columns)
        find_rating(&vvu, 0, true) *
        find_rating(&vvu, 0, false)
    })
}

fn process(file: Result<String>, is_part_a: bool) -> Result<ProcessingResult> {
    let res = file.map(|inp| {
        inp.trim()
            .lines()
            .map(|line| line.trim().chars().map(|c| c.to_digit(10).expect("Could not convert char to digit") as u8).collect::<Vec<u8>>())
            .collect::<Vec<Vec<u8>>>()});
            if is_part_a {
                res.map(|vvu| {
                    let h = vvu.len();
                    let w = vvu[0].len();
                    let vu: Vec<u8> = vvu.into_iter().flat_map(|v| v).collect();
                    ProcessingResult::A(Board::new(h,w,vu))
                })
            } else {
                res.map(|vvu| { ProcessingResult::B(vvu)})
            }
}

fn find_rating(data: &Vec<Vec<u8>>, position_to_check: usize, looking_for_oxygen_rating: bool) -> i64 {
    if data.len() == 1 {
        // if only one line is left, that's our result, convert to i64, bubble it up through return
        vu_to_i64(&data[0])
    } else {
        // for position_to_check => 
        //      1. make vector of bits in that position,
        let bits: Vec<u8> = (0..data.len()).map(|i| data[i][position_to_check]).collect();
        //      2. partition that vector,
        let (zeroes, ones): (Vec<u8>, Vec<u8>) = bits.into_iter().partition(|&x| x == 0);
        //      3. compare lengths
        // if       looking_for_oxygen_rating and lengths are equal, use 1, otherwise most common
        // if not   looking_for_oxygen_rating and lengths are equal, use 0, otherwise least common
        let predicate: u8 = match (zeroes.len().cmp(&ones.len()), looking_for_oxygen_rating) {
            (std::cmp::Ordering::Less, true) => 1,
            (std::cmp::Ordering::Less, false) => 0,
            (std::cmp::Ordering::Greater, true) => 0,
            (std::cmp::Ordering::Greater, false) => 1,
            (std::cmp::Ordering::Equal, true) => 1,
            (std::cmp::Ordering::Equal, false) => 0,
        };
        //      4. filter out only data lines containing that bit at that position, then recurse
        find_rating(&data.iter().cloned().filter(|x| x[position_to_check] == predicate).collect(), position_to_check+1, looking_for_oxygen_rating)
    }
}

fn vu_to_i64(vu: &[u8]) -> i64 {
    vu.into_iter().fold(0i64, |mut acc, &x| {
        acc = (acc << 1) | x as i64;
        acc
    })
}

#[test]
fn test_03_a() {
    let data = r#"00100
    11110
    10110
    10111
    10101
    01111
    00111
    11100
    10000
    11001
    00010
    01010"#;
    let result = part_a(process(Ok(data.to_string()), true)).expect("Something went wrong in this test.");
    assert_eq!(result, 198);
}

#[test]
fn test_03_b() {
    let data = r#"00100
    11110
    10110
    10111
    10101
    01111
    00111
    11100
    10000
    11001
    00010
    01010"#;
    let result = part_b(process(Ok(data.to_string()), false)).expect("Something went wrong in this test.");
    assert_eq!(result, 230);
}
