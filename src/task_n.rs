use super::*;

pub(crate) fn task_YYY_a() -> Result<ZZZ> {
    let contents = fs::read_to_string("data/YYY.txt")?;
    
    part_a(process(&contents))
}

pub(crate) fn task_YYY_b() -> Result<ZZZ> {
    let contents = fs::read_to_string("data/YYY.txt")?;
    
    part_b(process(&contents))
}

fn part_a(inp: Result<Vec<XXX>>) -> Result<ZZZ> {
    inp.map(|lines| {
        
    })
}

fn part_b(inp: Result<Vec<XXX>>) -> Result<ZZZ> {
    inp.map(|lines| {
        
    })
}

fn process(inp: &str) -> Result<Vec<XXX>> {
    inp.trim()
        .lines()
        .map(|line| {
            
        })
        .collect::<Result<Vec<_>>>()
}

#[test]
fn test_YYY_a() {
    let data = r#""#;
    let result = part_a(process(data)).expect("Something went wrong in this test.");
    assert_eq!(false, true);
}

#[test]
fn test_YYY_b() {
    let data = r#""#;
    let result = part_b(process(data)).expect("Something went wrong in this test.");
    assert_eq!(false, true);
}
