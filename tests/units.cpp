// aoc2021/tests/test_01.cpp
#include <task_01.hpp>

#define BOOST_TEST_MODULE task_01 test
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(test_01)

BOOST_AUTO_TEST_CASE(pos_pos)
{
    int a{100}, b{200};
    BOOST_CHECK_EQUAL(a + b, add2(a, b));
}

BOOST_AUTO_TEST_CASE(pos_neg)
{
    int a{100}, b{-200};
    BOOST_CHECK_EQUAL(a + b, add2(a, b));
}

BOOST_AUTO_TEST_CASE(neg_pos)
{
    int a{-100}, b{200};
    BOOST_CHECK_EQUAL(a + b, add2(a, b));
}

BOOST_AUTO_TEST_CASE(neg_neg)
{
    int a{-100}, b{-200};
    BOOST_CHECK_EQUAL(a + b, add2(a, b));
}

BOOST_AUTO_TEST_SUITE_END()
